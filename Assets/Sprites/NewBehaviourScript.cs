﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public GameObject d1;
    public GameObject d2;
    void Update()
    {
        if (d1.activeSelf)
        {
            d2.SetActive(true);
        }
    }
}
