﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateScript : MonoBehaviour
{
    public GameObject myobject;
    public bool activateme;

    void Update()
    {
        if (activateme == true)
        {
            myobject.SetActive(true);
        }
        else
        {
            myobject.SetActive(false);
        }
    }
}
