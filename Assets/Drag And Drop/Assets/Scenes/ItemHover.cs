﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHover : MonoBehaviour
{
    public GameObject logotext;

    public void Start()
    {
        logotext.SetActive(false);
    }
    public void OnMouseOver()
    {
        logotext.SetActive(true);
    }
    public void OnMouseExit()
    {
        logotext.SetActive(false);
    }
}

