﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class EquipmentSlot1 : MonoBehaviour
{
	protected DropArea DropArea;

	protected virtual void Awake()
	{
		DropArea = GetComponent<DropArea>() ?? gameObject.AddComponent<DropArea>();
		DropArea.OnDropHandler += OnItemDropped;
	}

	private void OnItemDropped(DraggableComponent draggable)
	{
		Scene currentScene = SceneManager.GetActiveScene();
		string sceneName = currentScene.name;
		if (sceneName == "1")
		{
			SceneManager.LoadScene("2");

		}
		if (sceneName == "2")
		{
			SceneManager.LoadScene("3");
		}
		draggable.transform.position = transform.position;
	}
}
